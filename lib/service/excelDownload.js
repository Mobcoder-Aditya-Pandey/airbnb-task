const Excel = require("exceljs");
const _ = require("lodash");
const fs = require("fs");
const { json } = require("body-parser");

function countryWiseCount(params) {
  let fileName = "countryWiseData" + new Date().toDateString() + ".xlsx";
  let filePath = "./download/" + fileName;

  const workbook = new Excel.Workbook();
  const worksheet = workbook.addWorksheet("Country Sheet");

  let col = [];

  // let col = []
  worksheet.getRow(1).values = ["Name", "Count"];
  worksheet.columns = col;

  _.map(params.data, function (user, key) {
    worksheet.columns = [
      {
        key: "Name",
        width: 18,
        style: { font: { name: "Calibri", family: 4, size: 12, bold: true } },
      },
      {
        key: "Count",
        width: 15,
        style: { font: { name: "Calibri", family: 4, size: 12, bold: true } },
      },
    ];

    worksheet.addRow({
      Name: user._id != null ? user._id : "",
      Count: user.count,
    }).font = { name: "Calibri", family: 4, size: 11, bold: false };
    // worksheet.addRow(user);
  });

  //worksheet.addRow({id: 2, name: 'Jane Doe', dob: new Date(1965, 1, 7)});

  // save under export.xlsx
  return workbook.xlsx.writeFile(filePath)
  .then(function (res) {
      console.log("File is written");
      return filePath
  })
}

module.exports = {
  countryWiseCount,
};
