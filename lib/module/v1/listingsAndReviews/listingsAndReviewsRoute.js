const router = require("express").Router();
const requestIp = require("request-ip");

const resHndlr = require("../../../responseHandler");
const middleware = require("../../../middleware");
const constants = require("../../../constant");
const listingsAndReviewsFacade = require("./listingsAndReviewsFacade");
const listingsAndReviewsValidators = require("./listingsAndReviewsValidators");

const constant = require("../../../constant");
//==============================================================

router.route("/getCountryCount").post(function (req, res) {
  let { country_code, download } = req.body;
  listingsAndReviewsFacade
    .getCountryCount({
      country_code,
      download,
    })
    .then(function (result) {
      resHndlr.sendSuccess(res, result, req);
    })
    .catch(function (err) {
      resHndlr.sendError(res, err, req);
    });
});

router.route("/getTopFive").post(function (req, res) {
  let { country_code } = req.body;
  listingsAndReviewsFacade
    .getTopFive({
      country_code,
    })
    .then(function (result) {
      resHndlr.sendSuccess(res, result, req);
    })
    .catch(function (err) {
      resHndlr.sendError(res, err, req);
    });
});

module.exports = router;
