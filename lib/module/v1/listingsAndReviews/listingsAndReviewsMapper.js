/**
 * This file will have request and response object mappings.
 */
var _ = require("lodash");
const contstants = require("../../../constant");
const config = require("../../../config");

function getCountryCount(params) {
  var respObj = {
    message: "Country list fetched successfully",
    result: params,
  };
  return respObj;
}

function getTopFive(params) {
  var respObj = {
    message: "Country list fetched successfully",
    result: params,
  };
  return respObj;
}

module.exports = {
  getCountryCount,
  getTopFive
};
