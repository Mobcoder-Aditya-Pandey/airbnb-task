"use strict";

//========================== Load Modules Start =======================
const appUtils = require("../../../appUtils");
//========================== Load internal modules ====================
const listingsAndReviewsDao = require("./listingsAndReviewsDao");

//========================== Load Modules End ==============================================

function getCountryCount(params) {
  return listingsAndReviewsDao.getCountryCount(params);
}

function getTopFive(params) {
  return listingsAndReviewsDao.getTopFive(params);
}
//========================== Export Module Start ==============================

module.exports = {
  getCountryCount,
  getTopFive,
};

//========================== Export Module End ===============================
