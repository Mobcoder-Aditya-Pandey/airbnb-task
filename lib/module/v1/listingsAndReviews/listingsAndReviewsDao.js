"use strict";
//========================== Load Modules Start =======================

//========================== Load internal modules ====================
var mongoose = require("mongoose");
var promise = require("bluebird");

var _ = require("lodash");
//========================== Load internal modules ====================
const listingsAndReviewsModel = require("./listingsAndReviewsModel");

// init user dao
let BaseDao = require("../../../dao/baseDao");
const listingsAndReviewsDao = new BaseDao(listingsAndReviewsModel);

//========================== Load Modules End ==============================================

function getCountryCount(params) {
  let pipeline = [];
  if (params.country_code) {
    let country_code = params.country_code.toUpperCase();
    pipeline.push({
      $match: {
        "address.country_code": country_code,
      },
    });

    pipeline.push({
      $group: {
        _id: "$address.country",
        country_code: { $first: "$address.country_code" },
        count: { $sum: 1 },
      },
    });
  } else {
    let query = {};
    pipeline.push({
      $match: query,
    });
    pipeline.push({
      $group: {
        _id: "$address.country",
        country_code: { $first: "$address.country_code" },
        count: { $sum: 1 },
      },
    });
  }
  return listingsAndReviewsDao.aggregate(pipeline);
}

function getTopFive(params) {
  console.log(params)
  let pipeline = [];
  pipeline.push({
    $match: {
      "address.country_code": params.country_code,
    },
  });
  pipeline.push({
    $group: {
      _id: "$review_scores.review_scores_rating",
    },
  });
  pipeline.push({
    $match: {
      _id: {
        $ne: null,
      },
    },
  });
  pipeline.push({
    $sort: { _id: -1 },
  });
  return listingsAndReviewsDao.aggregate(pipeline);
}
//========================== Export Module Start ==============================

module.exports = {
  getCountryCount,
  getTopFive,
};

//========================== Export Module End ===============================
