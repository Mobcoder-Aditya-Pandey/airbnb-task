// Importing mongoose
var mongoose = require("mongoose");
var constants = require("../../../constant");

var Schema = mongoose.Schema;
var listingsAndReviews;

var listingsAndReviewsSchema = new Schema({
  address: {
    street: { type: String },
    suburb: { type: String },
    country: { type: String },
    country_code: { type: String },
  },
  name:{type:String},
  status: {
    type: Number,
    default: 1,
  },
  isDelete: {
    type: Number,
    default: 0,
  },
  created: {
    type: Date,
    default: Date.now,
  },
  updated: {
    type: Date,
  },
});

//Export user module
listingsAndReviews = module.exports = mongoose.model(
  constants.DB_MODEL_REF.LISTINGSANDREVIEWS,
  listingsAndReviewsSchema
);
