//========================== Load Modules Start ===========================

//========================== Load external Module =========================
var _ = require("lodash");

//========================== Load Internal Module =========================
var appUtils = require("../../../appUtils");
var constant = require("../../../constant");
var exceptions = require("../../../customException");

//========================== Load Modules End =============================

//========================== Export Module Start ===========================

var validateDetails = function (req, res, next) {
  var { country_code } = req.body;
  var {} = req.headers;
  var errors = [];
  if (_.isEmpty(country_code)) {
    errors.push({
      fieldName: "country_code",
      message: constant.MESSAGES.KEY_CANT_EMPTY.replace(
        "{{key}}",
        "country_code"
      ),
    });
  }

  if (errors && errors.length > 0) {
    validationError(errors, next);
  }
  next();
};

var validationError = function (errors, next) {
  if (errors && errors.length > 0) {
    return next(
      exceptions.getCustomErrorException(
        constant.MESSAGES.validationError,
        errors
      )
    );
  }
  next();
};

module.exports = {
  validateDetails,
};
//========================== Export module end ==================================
