"use strict";

//========================== Load Modules Start =======================

//========================== Load external modules ====================
// Load user service
var _ = require("lodash");
var Promise = require("bluebird");
var ip = require("ip");
var vCardsJS = require("vcards-js");
//========================== Load internal modules ====================

const listingsAndReviewsService = require("./listingsAndReviewsService");
const listingsAndReviewsMapper = require("./listingsAndReviewsMapper");
const xlsDownload = require("../../../service/excelDownload");

const appUtils = require("../../../appUtils");
const redisSession = require("../../../redisClient/session");
const customException = require("../../../customException");
const emailService = require("../../../service/sendgrid_email");
const constant = require("../../../constant");
const config = require("../../../config");

//========================== Load Modules End ==============================================

function getCountryCount(params) {
  return listingsAndReviewsService.getCountryCount(params).then((res) => {
    if (params.download == 1 || params.download == "1") {
      return xlsDownload
        .countryWiseCount({ data: res })
        .then(function (result) {
          return { file: result };
        });
    } else {
      return listingsAndReviewsMapper.getCountryCount(res);
    }
  });
}

function getTopFive(params) {
  return listingsAndReviewsService.getTopFive(params).then((res) => {
    return listingsAndReviewsMapper.getTopFive(res);
  });
}

//========================== Export Module Start ==============================

module.exports = {
  getCountryCount,
  getTopFive,
};

//========================== Export Module End ================================
