var express = require("express");
var router = express.Router();

const airbnbRoute = require("./listingsAndReviews/listingsAndReviewsRoute");

//========================== Export Module Start ==========================

//API version 1
router.use("/airbnb", airbnbRoute);

module.exports = router;
//========================== Export Module End ============================
